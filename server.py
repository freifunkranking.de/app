#!/usr/bin/python3

from flask import Flask
from flask import render_template
from flask import jsonify
import requests
import json
import ff_net_worth_calculator as nwc

app = Flask(__name__)

ignored_communities = ['kassel', 'waldheim', 'bochum']
communities_json = 'https://api.freifunk.net/data/ffSummarizedDir.json'
model_information = nwc.load_devices_json(nwc.get_data_path('devices.json'))


@app.route("/")
def hello():
    return render_template('index.html')


@app.route('/overview.nocache.json')
def overview_json():
    community_scores = fetch_community_json()
    return jsonify(community_scores)


@app.route('/overview.json')
def overview_json_cached():
    try:
        with open('overview_cache.json', 'r') as file:
            community_scores = json.loads(file.read())
    except FileNotFoundError:
        community_scores = fetch_community_json()
    return jsonify(community_scores)


def fetch_community_json():
    with open('communities.json', 'r') as file:
        nodelist_urls = json.loads(file.read())

    community_scores = {}

    for community in nodelist_urls:
        print(nodelist_urls[community])

        if community in ignored_communities:
            continue

        # hateful merging of multiple data files
        score = {'loss': 0, 'models': {}}
        for url in nodelist_urls[community]:
            tmp = calculate_score(url)
            score['loss'] += tmp['loss']
            for model, values in tmp['models'].items():
               if model not in score['models'].keys():
                   score['models'][model] = {'count': 0, 'loss': 0}
               score['models'][model]['count'] += values['count']
               score['models'][model]['loss'] += values['loss']

        print(community + ' has score ' + str(score))
        if score['loss'] != 0:
            community_scores[community] = score

    with open('overview_cache.json', 'w') as handle:
        json.dump(community_scores, handle)

    return community_scores


def calculate_score(url):
    meshviewer_json = []
    data = {'models': {}, 'loss': 0}
    total_loss = 0

    try:
        meshviewer_json += nwc.load(url, None)
    except:
        return data

    community_information = nwc.gather_information(model_information, meshviewer_json)

    for model in community_information:
        if model["total"] == -1:
            continue

        total_loss += model["total"]
        data['models'][model["model"]] = {'count': model["count"], 'loss': model["total"]}

    data['loss'] = total_loss
    return data


fetch_community_json()
print('= Server ready =')
if __name__ == '__main__':
    app.run(debug=True)
